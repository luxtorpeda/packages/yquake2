#!/bin/bash

source env.sh

set -x
set -e

log_environment
prepare_dist_dirs "$STEAM_APP_ID_LIST"

# build Yamagi Quake II
#
pushd "source"
# Workaround for glibc < 2.17; in newer glibc it's no longer necessary
# to link to librt when using functions from time.h.
echo 'release/q2ded : LDFLAGS += -l:librt.a' > config.mk
make -j "$(nproc)"
popd

mkdir -p "2320/dist/license/"
mkdir -p "2320/dist/baseq2/"
cp -v   "quake2.sh"                    "2320/dist/"
cp -v   "default.lux.cfg"              "2320/dist/baseq2/yq2.cfg"
cp -v   "source/LICENSE"               "2320/dist/license/LICENSE.yquake2.txt"
cp -v   "source/stuff/icon/Quake2.svg" "2320/dist/"
cp -rfv "source/release/"*             "2320/dist/"
